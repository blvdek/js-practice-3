// Data
async function browserDataProvider (param) {
    return new Promise((resolve, reject) => {
        resolve(prompt(param, ""));
    });
}

async function consoleDataProvider(param) {
    let readline = require("readline");
    let rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.setPrompt('guess> ');
    rl.prompt();
    return new Promise((resolve, reject) => {
            rl.question(param, answer => {
            resolve(answer);
            rl.close();
        });
    });
}

// Logic
function parseDate(userDate) {
    let date = userDate.split(".");
    return new Date(`${parseInt(date[2])}-${parseInt(date[1])}-${parseInt(date[0]) + 1}`);
}

function dateDifference(userDate) {
    let date = parseDate(userDate);
    let toDayDate = new Date();
    return Math.round(Math.abs((toDayDate - date) / (1000 * 3600 * 24)));
}

// Client
function isNodeJsEnvironment() {
    return typeof window === `undefined`;
}

if(isNodeJsEnvironment()) {
    dataProvider = consoleDataProvider("Input your date of birth in the format \"Day.Month.Year\": ");
} else{
    dataProvider = browserDataProvider("Input your date of birth in the format \"Day.Month.Year\": ");
}

// Вероятно, можно сделать короче
if (isNodeJsEnvironment()) {
    dataProvider.then((answer) => {
        let days = dateDifference(answer);
        console.log((isNaN(days) ? "You input incorrect date." : `${days} days between these dates.`));
    })
} else {
    dataProvider.then((answer) => {
        let days = dateDifference(answer);
        alert((isNaN(days) ? "You input incorrect date." : `${days} days between these dates.`));
    })
}
